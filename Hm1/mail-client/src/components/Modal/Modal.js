import React, { Component } from 'react'
import "./Modal.scss";
import Button from "../Button/Button"

export default class Modal extends Component {
    render() {
        const { header, text, closeButton, onClick, actions } = this.props;

        return (
            <>
                <div className="darkGround" onClick={onClick}></div>
                <div className="modal" onClick={this.click}>
                    <header className="header">
                        {header}
                        {actions}
                        {closeButton && <Button text="X" backgroundColor="inherit" onClick={onClick} className="closeBtn" />}
                    </header>
                    <main className="main">
                        {text}
                        <div className="btns">
                            <Button backgroundColor="#523637" text="Ok" className="openBtn" onClick={onClick} />
                            <Button backgroundColor="#523637" text="Cancel" className="openBtn" onClick={onClick} />
                        </div>
                    </main>
                </div>
            </>
        )
    }
}

