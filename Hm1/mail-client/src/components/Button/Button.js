import React, { PureComponent } from 'react';
import "./Button.scss";

export default class Button extends PureComponent {
    render() {
        const { text,backgroundColor,onClick,className } = this.props
        return (
            <>
                <button style={{backgroundColor:backgroundColor}} className={className} onClick={onClick}>{text}</button>
            </>
        )
    }
}
