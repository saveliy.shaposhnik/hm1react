import './App.css';
import React from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";


class App extends React.Component {

  constructor() {
    super();
    this.state = {
      isActive: false,
      header:"",
      title:"",
    }
  }

  closeModal = () => {
    this.setState({ isActive: false });
  }

  openFirstModal = () => {
    this.setState({isActive:true,header:"Do you want to delete this file?",title:"Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?" });
  }
  openSecondModal = () => {
    this.setState({isActive:true,header:"Are you sure want to open this?",title:"Once you open this you will be surprised" });
  }

  render() {
    const {header,title,isActive} = this.state;
    return (
      <div className="App">

        <Button text="Open First Modal" backgroundColor="#bb8918" onClick={this.openFirstModal} className="openBtn" />

        <Button text="Open Second Modal" backgroundColor="#523637" onClick={this.openSecondModal} className="openBtn" />

        {isActive && <Modal header={header} text={title} closeButton={true} onClick={this.closeModal}/>} 
      </div>
    );
  }
}

export default App;
